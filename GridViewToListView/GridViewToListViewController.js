/**
 * Created by hardikranjan on 31/10/20.
 */

({
    handleSwitchView : function(component, event, helper) {
        component.set('v.view', event.getSource().get('v.name'));
    }
});